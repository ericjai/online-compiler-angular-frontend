import { TestBed } from '@angular/core/testing';

import { CppdataService } from './cppdata.service';

describe('CppdataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CppdataService = TestBed.get(CppdataService);
    expect(service).toBeTruthy();
  });
});
