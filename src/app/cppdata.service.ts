import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CppdataService {

  constructor(private http:HttpClient) { }

  sendcppInputToBackend(input)
  {
    
    return this.http.post("http://localhost:8080/cppinput",input,{responseType:"text"});

  }
}
