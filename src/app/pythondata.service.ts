import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PythondataService {

  constructor(private http:HttpClient) { }

  sendPythonInputToBackend(input)
  {
    
    return this.http.post("http://localhost:8080/pythoninput",input,{responseType:"text"});

  }
}
