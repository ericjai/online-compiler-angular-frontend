import { TestBed } from '@angular/core/testing';

import { CompilerDataService } from './compiler-data.service';

describe('CompilerDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CompilerDataService = TestBed.get(CompilerDataService);
    expect(service).toBeTruthy();
  });
});
