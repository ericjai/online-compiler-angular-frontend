import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CppCompilerComponent } from './cpp-compiler/cpp-compiler.component';
import { JavaCompilerComponent } from './java-compiler/java-compiler.component';
import { MainComponent } from './main/main.component';
import { PythonCompilerComponent } from './python-compiler/python-compiler.component';


const routes: Routes = [
  {path:"", component:MainComponent,children:[
    {path:"javacompiler",component:JavaCompilerComponent},
    {path:"pythoncompiler",component:PythonCompilerComponent},
    {path:"cppcompiler",component:CppCompilerComponent}
  ]},
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
