import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import * as ace from "ace-builds";
import { CppdataService } from '../cppdata.service';

@Component({
  selector: 'app-cpp-compiler',
  templateUrl: './cpp-compiler.component.html',
  styleUrls: ['./cpp-compiler.component.css']
})
export class CppCompilerComponent implements OnInit {
  @ViewChild('editor',{static:true }) private editor: { nativeElement: string | Element; };
  aceEditor;

  constructor(private cppdataservice:CppdataService) { }

  ngOnInit() {
    ace.config.set("fontSize", "16px");
    ace.config.set(
      "basePath",
      "https://unpkg.com/ace-builds@1.4.12/src-noconflict"
    );
    this.aceEditor = ace.edit(this.editor.nativeElement);   
    this.aceEditor.setTheme("ace/theme/twilight");
    this.aceEditor.session.setMode("ace/mode/java");
    this.aceEditor.session.setValue(`#include <iostream>
    using namespace std;
      int main() {
      cout << "Hello World!";
      return 0;
    } `);   
  }

  responseData;

  runProgram() 
  {
    const  formData = new FormData();
    formData.append("editorinput",this.aceEditor.getSession().getValue());  
    
    this.cppdataservice.sendcppInputToBackend(formData).subscribe(res=>{  
      this.responseData =res;
      console.log(res);
    });
    //console.log(this.aceEditor.getSession().getValue());
  }
  clearIde()
  {
    this.aceEditor.session.setValue("");

  }

}
