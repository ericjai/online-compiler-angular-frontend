import { ElementRef } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import * as ace from "ace-builds";
import { cwd } from 'process';
import { CompilerDataService } from '../compiler-data.service';

@Component({
  selector: 'app-java-compiler',
  templateUrl: './java-compiler.component.html',
  styleUrls: ['./java-compiler.component.css']
})
export class JavaCompilerComponent implements OnInit {
  @ViewChild('editor', { static: true }) private editor: ElementRef<HTMLElement>;
   
  aceEditor;
  

  constructor(private compilerDataService:CompilerDataService) 
  {

  }

  ngOnInit() {

    ace.config.set("fontSize", "16px");
    ace.config.set(
      "basePath",
      "https://unpkg.com/ace-builds@1.4.12/src-noconflict"
    );
    this.aceEditor = ace.edit(this.editor.nativeElement);   
    this.aceEditor.setTheme("ace/theme/twilight");
    this.aceEditor.session.setMode("ace/mode/java");
    this.aceEditor.session.setValue(`class Main{
      public static void main(String args[])
      {
        System.out.println("HelloWorld");
      }
    }`);          
  }
  responseData;

  runProgram() 
  {
    const  formData = new FormData();
    formData.append("editorinput",this.aceEditor.getSession().getValue());  
    
    this.compilerDataService.sendInputToBackend(formData).subscribe(res=>{  
      this.responseData =res;
      console.log(res);
    });
    console.log(this.aceEditor.getSession().getValue());
  }
  clearIde()
  {
    this.aceEditor.session.setValue("");

  }
}























































































































































































































































