import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import * as ace from "ace-builds";
import { PythondataService } from '../pythondata.service';

@Component({
  selector: 'app-python-compiler',
  templateUrl: './python-compiler.component.html',
  styleUrls: ['./python-compiler.component.css']
})
export class PythonCompilerComponent implements OnInit {
  @ViewChild('editor',{static:true }) private editor: { nativeElement: string | Element; };
  aceEditor;

  constructor(private pythonDataService:PythondataService) { }

  ngOnInit() {
    ace.config.set("fontSize", "16px");
    ace.config.set(
      "basePath",
      "https://unpkg.com/ace-builds@1.4.12/src-noconflict"
    );
    this.aceEditor = ace.edit(this.editor.nativeElement);
    this.aceEditor.setTheme("ace/theme/twilight");
    this.aceEditor.session.setMode("ace/mode/python")
    this.aceEditor.session.setValue(`print("Hello World")`);
  }
  responseData;
  runProgram()
  {
    const  formData = new FormData();
    formData.append("editorinput",this.aceEditor.getSession().getValue());  
    
    this.pythonDataService.sendPythonInputToBackend(formData).subscribe(res=>{  
      this.responseData =res;
      console.log(res);
    });
    console.log(this.aceEditor.getSession().getValue());
  }
  clearIde()
  {
    this.aceEditor.session.setValue("");
  }


}
