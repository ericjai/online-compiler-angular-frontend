import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CompilerDataService {

  constructor(private http:HttpClient) { }
  

  sendInputToBackend(input)
  {
    
    return this.http.post("http://localhost:8080/getinputfromfrontend",input,{responseType:"text"});

  }
  sendPythonInputToBackend(input)
  {
    
    return this.http.post("http://localhost:8080/pythoninput",input,{responseType:"text"});

  }
}

 