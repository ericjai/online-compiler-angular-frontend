import { TestBed } from '@angular/core/testing';

import { PythondataService } from './pythondata.service';

describe('PythondataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PythondataService = TestBed.get(PythondataService);
    expect(service).toBeTruthy();
  });
});
